function oddEvenChecker(num) {
	typeof num === "number"
	? (num % 2 === 0 ? console.log("The number is even") : console.log("The number is odd"))
	: alert("Invalid Input.")
};

function budgetChecker(budget) {
	typeof budget === "number" 
	? (budget > 40000 ? console.log("You are over the budget") : console.log("You have resources left."))
	: alert("Invalid Input.");
};

oddEvenChecker(1);
oddEvenChecker(2);
oddEvenChecker("Kama Sammohana");
budgetChecker(42000);
budgetChecker(36000);
budgetChecker("Voyager took my Castoria funds");